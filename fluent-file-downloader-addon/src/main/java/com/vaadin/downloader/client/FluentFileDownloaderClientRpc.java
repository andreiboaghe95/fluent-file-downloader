package com.vaadin.downloader.client;

import com.vaadin.shared.communication.ClientRpc;

public interface FluentFileDownloaderClientRpc extends ClientRpc {

    /**
     * Forces the fluentFileDownloader to send the Resource to the client.
     */
    void download();
}
