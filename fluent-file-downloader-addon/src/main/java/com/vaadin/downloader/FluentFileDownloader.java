package com.vaadin.downloader;

import com.vaadin.downloader.client.FluentFileDownloaderClientRpc;
import com.vaadin.server.Extension;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.Resource;
import com.vaadin.ui.UI;

import java.util.Collection;
import java.util.Optional;

/**
 * This FileDownloader provides more useful functionality for {@link Resource} downloading.
 * There is no necessity in manual FileDownloader creation and configuration.
 */
public class FluentFileDownloader extends FileDownloader {

    private FluentFileDownloader(Resource resource) {
        super(resource);
        extend(UI.getCurrent());
    }

    private void doDownload() {
        getRpcProxy(FluentFileDownloaderClientRpc.class).download();
    }

    public static void download(Resource resource) {
        download(resource, true);
    }

    public static void download(Resource resource, boolean overrideContentType) {
        FluentFileDownloader fileDownloader = getFileDownloader(resource);
        fileDownloader.setFileDownloadResource(resource);
        fileDownloader.setOverrideContentType(overrideContentType);
        fileDownloader.doDownload();
    }

    /**
     * Gets a FluentFileDownloader extension from the current UI if such exists,
     * or creates a new one.
     * <p>
     * <b>Note:</b><br/>
     * Only one FluentFileDownloader instance is allowed per UI.
     * </p>
     *
     * @param resource the resource to download
     * @return FluentFileDownloader instance for the current UI
     */
    private static FluentFileDownloader getFileDownloader(Resource resource) {
        UI currentUI = UI.getCurrent();
        if (currentUI == null) {
            throw new IllegalArgumentException("UI instance is required");
        }

        Collection<Extension> uiExtensions = currentUI.getExtensions();
        Optional<Extension> fileDownloader = uiExtensions.stream()
                .filter(FluentFileDownloader.class::isInstance)
                .findFirst();

        return fileDownloader
                .map(extension -> ((FluentFileDownloader) extension))
                .orElseGet(() -> new FluentFileDownloader(resource));
    }
}
