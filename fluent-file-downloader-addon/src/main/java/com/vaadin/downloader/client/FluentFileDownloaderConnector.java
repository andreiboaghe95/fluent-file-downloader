package com.vaadin.downloader.client;


import com.vaadin.client.ServerConnector;
import com.vaadin.client.extensions.FileDownloaderConnector;
import com.vaadin.downloader.FluentFileDownloader;
import com.vaadin.shared.ui.Connect;

@Connect(FluentFileDownloader.class)
public class FluentFileDownloaderConnector extends FileDownloaderConnector {

    @Override
    protected void init() {
        super.init();
        registerRpc(FluentFileDownloaderClientRpc.class, this::download);
    }

    @Override
    protected void extend(ServerConnector target) {
        // Do not add clickHandler to the target Widget.
        // We don't need to handle ClickEvents (that's crazy).
    }

    private void download() {
        // The original FileDownloaderConnector handles ClickEvent and opens
        // IFrameElement with the downloaded Resource (it doesn't requires ClickEvent instance).
        // So we could call it with null arg (in order to avoid code copying :) )
        onClick(null);
    }
}
